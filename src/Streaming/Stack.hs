{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Streaming.Stack where

import Control.Monad.Base
import Protolude
import Streaming (Of, Stream, hoist)
import qualified Streaming.Prelude as S

-- | build the stack of streams from the yielded types
type family StackOfTypes (m :: Type -> Type) (c :: [Type]) :: (Type -> Type) where
  StackOfTypes m '[] = m
  StackOfTypes m (x ': xs) = Stream (Of x) (StackOfTypes m xs)

-- | extract the yielded types from a stack
type family TypesOfStack (s :: Type -> Type) :: [Type] where
  TypesOfStack (Stream (Of x) xs) = x ': TypesOfStack xs
  TypesOfStack t = '[]

-- compute the first non streaming layer
type family StreamBase (s :: Type -> Type) :: (Type -> Type) where
  StreamBase (Stream (Of x) xs) = StreamBase xs
  StreamBase t = t

type HasStack m = StackOfTypes (StreamBase m) (TypesOfStack m) ~ m


-- |  natural transformation  on the base of the stream stack
class HoistStreamBase (m :: Type -> Type) (n :: Type -> Type) (c :: [Type]) where
  hoistStreamBase
    :: Proxy m
    -> Proxy c
    -> (forall b. m b -> n b)
    -> StackOfTypes m c a
    -> StackOfTypes n c a

instance HoistStreamBase m n '[] where
  hoistStreamBase _ _ f = f

instance
  ( StackOfTypes m (x : xs) ~ t (StackOfTypes m xs)
  , HoistStreamBase m n xs
  , Monad (StackOfTypes m xs)
  )
  => HoistStreamBase m n (x ': xs)
  where
  hoistStreamBase pm _ f = hoist $ hoistStreamBase pm (Proxy @xs) f

-- | perform a natural transformation  on the monad at the bottom of a stack of Streams
hoistStreamBaseP
  :: forall m n a.
  ( n ~ StackOfTypes (StreamBase n) (TypesOfStack m)
  , HoistStreamBase (StreamBase m) (StreamBase n) (TypesOfStack m)
  , StackOfTypes (StreamBase m) (TypesOfStack m) ~ m
  )
  => (forall x. (StreamBase m) x -> (StreamBase n) x)
  -> m a
  -> n a
hoistStreamBaseP = hoistStreamBase (Proxy @(StreamBase m)) (Proxy @(TypesOfStack m))

-- | lift from the base of the stack
class LiftStreamBase (m :: Type -> Type) (c :: [Type]) where
  liftStreamBase
    :: Proxy m
    -> Proxy c
    -> m a
    -> StackOfTypes m c a

instance LiftStreamBase m '[] where
  liftStreamBase _ _ = identity

instance
  ( StackOfTypes m (x : xs) ~ t (StackOfTypes m xs)
  , LiftStreamBase m xs
  , Monad (StackOfTypes m xs)
  )
  => LiftStreamBase m (x ': xs)
  where
  liftStreamBase pm _ x = lift $ liftStreamBase pm (Proxy @xs) x

-- | lift an action from the monad at the base of the stack of Streams
liftStreamBaseP
  :: forall m a. (HasStreamBase m) => StreamBase m a
  -> m a
liftStreamBaseP = liftStreamBase (Proxy @(StreamBase m)) (Proxy @(TypesOfStack m))

type HasStreamBase m =
  ( LiftStreamBase (StreamBase m) (TypesOfStack m)
  , HasStack m
  )

-- | yield on the first layer matching the yielded type
class YieldA q (m :: Type -> Type) (c :: [Type]) where
  yieldA
    :: Proxy m
    -> Proxy c
    -> q
    -> StackOfTypes m c ()

instance
  {-# OVERLAPS #-}
  ( Monad (StackOfTypes m xs)
  )
  => YieldA q m (q ': xs)
  where
  yieldA _ _ = S.yield

instance
  ( StackOfTypes m (x : xs) ~ t (StackOfTypes m xs)
  , YieldA q m xs
  , Monad (StackOfTypes m xs)
  )
  => YieldA q m (x ': xs)
  where
  yieldA pm _ = lift . yieldA pm (Proxy @xs)

type HasStream q m =
  ( YieldA q (StreamBase m) (TypesOfStack m)
  , StackOfTypes (StreamBase m) (TypesOfStack m) ~ m
  )

yieldP :: forall m q. (HasStream q m) => q -> m ()
yieldP = yieldA (Proxy @(StreamBase m)) (Proxy @(TypesOfStack m))

testYieldP :: StackOfTypes IO '[Char, Text] ()
testYieldP = do
  yieldP 'c'
  yieldP ("c" :: Text)
  yieldP 'c'

---- map the elements of  a stream layer based on the yielded types ---------------------
class MapStream q q' m c c' where
  mapStream :: Proxy m -> Proxy c -> Proxy c' -> (q -> q') -> StackOfTypes m c a -> StackOfTypes m c' a

instance (Monad (StackOfTypes m xs)) => MapStream q q' m (q : xs) (q' : xs) where
  mapStream _ _ _ = S.map

instance
  ( MapStream q q' m xs xs'
  , Monad (StackOfTypes m xs)
  )
  => MapStream q q' m (x : xs) (x : xs')
  where
  mapStream pm _ _ f x = hoist (mapStream pm (Proxy @xs) (Proxy @xs') f) x

mapStreamP
  :: forall m m' q q' a.
  ( MapStream q q' (StreamBase m) (TypesOfStack m) (TypesOfStack m')
  , m ~ StackOfTypes (StreamBase m) (TypesOfStack m)
  , StackOfTypes (StreamBase m) (TypesOfStack m') ~ m'
  )
  => (q -> q')
  -> m a
  -> m' a
mapStreamP = mapStream (Proxy @(StreamBase m)) (Proxy @(TypesOfStack m)) (Proxy @(TypesOfStack m'))
